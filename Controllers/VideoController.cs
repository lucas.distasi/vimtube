﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Headers;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using VimTube.Entities;
using VimTube.Repositories;

namespace VimTube.Controllers
{
    [ApiController]
    [Route("vimtube/videos")]
    public class VideoController
    {
        private readonly VideoRepository _repository;

        public VideoController(VideoRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [ResponseCache(Duration = 60)]
        [Authorize(Roles = "ADMIN, VIEWER")]
        public async Task<ActionResult<IEnumerable<Video>>> GetAllVideos()
        {
            return await _repository.FindAll();
        }

        [HttpGet]
        [ResponseCache(Duration = 60)]
        [Route("{id}")]
        [Authorize(Roles = "ADMIN, VIEWER")]
        public async Task<ActionResult<Models.Video>> GetVideo(int id)
        {
            return await _repository.FindOne(id);
        }

        [HttpPost]
        [Route("add")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Video>> AddVideo(Video video)
        {
            return await _repository.AddVideo(video);
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Video>> UpdateVideo(int id, Video video)
        {
            video.IdVideo = id;
            return await _repository.UpdateVideo(id, video);
        }

        [HttpPatch]
        [Route("{id}/description")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Video>> UpdateVideoDescription(int id, [FromBody]JsonPatchDocument<Video> descriptionPatch)
        {
            return await _repository.UpdateVideoDescription(id, descriptionPatch);
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Video>> DeleteVideo(int id)
        {
            return await _repository.DeleteVideo(id);
        }

    }
}