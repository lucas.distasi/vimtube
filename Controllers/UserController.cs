﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using VimTube.Entities;
using VimTube.Repositories;

namespace VimTube.Controllers
{
    [ApiController]
    [Route("vimtube/users")]
    public class UserController
    {
        private readonly UserRepository _repository;

        public UserController(UserRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [ResponseCache(Duration = 60)]
        [Route("{id}")]
        [ActionName("GetUser")]
        [Authorize(Roles = "ADMIN, VIEWER")]
        public async Task<ActionResult<Models.User>> GetUser(int id)
        {
            return await _repository.FindOne(id);
        }

        [HttpGet]
        [ResponseCache(Duration = 60)]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<IEnumerable<User>>> GetAllUsers()
        {
            return await _repository.FindAll();
        }

        [HttpPost]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<User>> AddUser(Models.User user)
        {
            return await _repository.AddUser(user);
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<User>> UpdateUser(int id, Models.User user)
        {
            return await _repository.UpdateUser(id, user);
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            return await _repository.DeleteUser(id);
        }
    }
}