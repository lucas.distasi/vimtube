﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VimTube.Models;
using VimTube.Repositories;

namespace VimTube.Controllers
{
    [ApiController]
    [AllowAnonymous]
    [Route("vimtube/login")]
    public class LoginController : ControllerBase
    {
        private readonly LoginRepository _loginRepository;

        public LoginController(LoginRepository loginRepository)
        {
            _loginRepository = loginRepository;
        }

        [HttpPost]
        public IActionResult Login(User user)
        {
            string identifier = string.IsNullOrEmpty(user.Email) ? user.Username : user.Email;
            return _loginRepository.Login(identifier, user.Password);
        }
    }
}