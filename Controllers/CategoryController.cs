﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using VimTube.Entities;
using VimTube.Repositories;

namespace VimTube.Controllers
{
    [ApiController]
    [Route("vimtube/categories")]
    public class CategoryController
    {
        private readonly CategoryRespository _repository;

        public CategoryController(CategoryRespository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [ResponseCache(Duration = 60)]
        [Authorize(Roles = "ADMIN, VIEWER")]
        public async Task<ActionResult<IEnumerable<Category>>> GetAllCategories()
        {
            return await _repository.FindAll();
        }

        [HttpGet]
        [ResponseCache(Duration = 60)]
        [Route("search/{category}")]
        [Authorize(Roles = "ADMIN, VIEWER")]
        public List<Models.Category> ListCategoriesWhichStartsWith(string category)
        {
            return _repository.ListCategoriesWhichStartsWith(category);
        }

        [HttpGet]
        [ResponseCache(Duration = 60)]
        [Route("{id}")]
        [Authorize(Roles = "ADMIN, VIEWER")]
        public async Task<ActionResult<Models.Category>> GetCategory(int id)
        {
            return await _repository.FindOne(id);
        }

        [HttpPost]
        [Route("add")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Category>> AddCategory(Category category)
        {
            return await _repository.AddCategory(category);
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Category>> EditCategory(int id, Category category)
        {
            category.IdCategory = id;
            return await _repository.UpdateCategory(id, category);
        }

        [HttpDelete]
        [Route("{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Category>> DeleteCategory(int id)
        {
            return await _repository.DeleteCategory(id);
        }
    }
}