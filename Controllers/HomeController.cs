﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace VimTube.Controllers
{
    [Route("[controller]")]
    [Route("index")]
    [Route("/")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        [AllowAnonymous]
        public string Get()
        {
            return "Home Controller";
        }
    }
}