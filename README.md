
Final .NET Training project.

<hr>

### Categories

| Action | Endpoint | Success | Error | Decription | Available for
| ------ | ------ | ------ | ------ | ------ | ------ |
| GET | /vimtube/categories | 200 | - | Get all categories | ADMIN - VIEWER
| GET | /vimtube/categories/search/\{name} | 200 | - | Get categories starting with \{name} | ADMIN - VIEWER
| GET | /vimtube/categories/\{ID} | 200 | 400 - 404 | Get category with \{id} | ADMIN - VIEWER
| POST | /vimtube/categories/add | 201 | 400 - 415 | Add new category | ADMIN
| PUT | /vimtube/categories/\{ID} | 201 | 400 - 404 | Modify category name | ADMIN
| DELETE | /vimtube/categories/\{ID} | 204 | 400 - 404 - 405 | Delete category | ADMIN

<br/>

### Videos

| Action | Endpoint | Success | Error | Decription | Available for
| ------ | ------ | ------ | ------ | ------ | ------ |
| GET | /vimtube/videos | 200 | - | Get all videos | ADMIN - VIEWER
| GET | /vimtube/videos/\{ID} | 200 | 400 - 404 | Get video with \{id} | ADMIN - VIEWER
| POST | /vimtube/videos/add | 201 | 400 - 415 | Add new video | ADMIN
| PUT | /vimtube/videos/\{ID} | 201 | 400 - 404 | Modify video information | ADMIN
| DELETE | /vimtube/videos/\{ID} | 204 | 400 - 404 - 405 | Delete video | ADMIN
| PATCH | /vimtube/videos/\{ID}/description | 200 | 400 - 404 | Modify video description | ADMIN

<br/>

### Users

| Action | Endpoint | Success | Error | Decription | Available for
| ------ | ------ | ------ | ------ | ------ | ------ |
| GET | /vimtube/users | 200 | - | Get all users | ADMIN - VIEWER
| GET | /vimtube/users/\{ID} | 200 | 400 - 404 | Get user with \{id} | ADMIN - VIEWER
| POST | /vimtube/users/add | 201 | 400 - 415 | Add new user | ADMIN - VIEWER
| PUT | /vimtube/users/\{ID} | 201 | 400 - 404 | Modify user information | ADMIN
| DELETE | /vimtube/users/\{ID} | 204 | 400 - 404 - 405 | Delete user | ADMIN

<br/>
<hr>
<br/>

# Categories

| Method | Endpoint |
| ------ | ------ |
| **GET** | */vimtube/categories* |
##### Request
```sh
None
```

##### Success Response
```sh
[
    {
        "name": "Romance",
        "videos": []
    },
    {
        "name": "Horror",
        "videos": [
            {
                "name": "A Nightmare on Elm Street",
                "description": "A Nightmare on Elm Street is a 1984 American slasher film written and directed by Wes Craven, and produced by Robert Shaye.",
                "duration": 91,
                "uploadedDate": "2020-04-19T22:39:09.3921347",
                "isAgeRestricted": false
            }
        ]
    },
    {
        "name": "Suspense",
        "videos": []
    }
]

```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **GET** | */vimtube/categories/hor* |
##### Request
```sh
None
```

##### Success Response
```sh
[
    {
        "name": "Horror",
        "videos": [
            {
                "name": "A Nightmare on Elm Street",
                "description": "A Nightmare on Elm Street is a 1984 American slasher film written and directed by Wes Craven, and produced by Robert Shaye.",
                "duration": 91,
                "uploadedDate": "2020-04-19T22:39:09.3921347",
                "isAgeRestricted": false
            }
        ]
    }
]
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **GET** | */vimtube/categories/1* |
##### Request
```sh
None
```

##### Success Response
```sh
{
	"name": "Horror",
	"videos": [
            {
		"name": "A Nightmare on Elm Street",
		"description": "A Nightmare on Elm Street is a 1984 American slasher film written and directed by Wes Craven, and produced by Robert Shaye.",
		"duration": 91,
		"uploadedDate": "2020-04-19T22:39:09.3921347",
		"isAgeRestricted": false
            }
        ]
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **POST** | */vimtube/categories/add* |
##### Request
```sh
{
    "name": "Horror"
}
```
##### Success Response
```sh
{
    "name": "Horror"
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **PUT** | */vimtube/categories/1* |
##### Request
```sh
{
    "name": "Suspense"
}
```
##### Success Response
```sh
{
    "name": "Suspense"
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **DELETE** | */vimtube/categories/1* |
##### Request
```sh
None
```
##### Success Response
```sh
None
```

<br/>

# Videos

| Method | Endpoint |
| ------ | ------ |
| **GET** | */vimtube/videos* |
##### Request
```sh
None
```
##### Success Response
```sh
[
    {
        "name": "Booksmart",
        "description": "Academic overachievers Amy and Molly thought keeping their noses to the grindstone gave them a leg up on their high school peers.",
        "duration": 105,
        "uploadedDate": "2020-04-20T16:21:08.2728934",
        "isAgeRestricted": false,
        "category": "Comedy"
    }
]
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **GET** | */vimtube/videos/1* |
##### Request
```sh
None
```
##### Success Response
```sh
{
    "name": "Booksmart",
    "description": "Academic overachievers Amy and Molly thought keeping their noses to the grindstone gave them a leg up on their high school peers.",
    "duration": 105,
    "uploadedDate": "2020-04-20T16:21:08.2728934",
    "isAgeRestricted": false,
    "category": "Comedy"
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **POST** | */vimtube/videos/add* |
##### Request
```sh
{
    "name": "Booksmart",
    "description": "Academic overachievers Amy and Molly thought keeping their noses to the grindstone gave them a leg up on their high school peers.",
    "duration": 105,
    "isAgeRestricted": false,
    "idCategory" : 1
}
```
##### Success Response
```sh
{
    "name": "Booksmart",
    "description": "Academic overachievers Amy and Molly thought keeping their noses to the grindstone gave them a leg up on their high school peers.",
    "duration": 105,
    "uploadedDate": "2020-04-20T16:21:08.2728934",
    "isAgeRestricted": false,
    "category": "Comedy"
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **PUT** | */vimtube/videos/1* |
##### Request
```sh
{
    "name": "Booksmart 2",
    "description": "Continuing the academic overachievers Amy and Molly thought keeping their noses to the grindstone gave them a leg up on their high school peers.",
    "duration": 90,
    "isAgeRestricted": false,
    "idCategory": 2
}
```
##### Success Response
```sh
{
    "name": "Booksmart 2",
    "description": "Continuing the academic overachievers Amy and Molly thought keeping their noses to the grindstone gave them a leg up on their high school peers.",
    "duration": 90,
    "uploadedDate": "2020-04-20T16:39:41.1347234Z",
    "isAgeRestricted": false,
    "category": "Romance"
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **DELETE** | */vimtube/videos/1* |
##### Request
```sh
None
```
##### Success Response
```sh
None
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **PATCH** | */vimtube/videos/1/description* |
##### Request
```sh
[{
    "op": "replace",
    "path": "/description",
    "value": "The academic overachievers Amy and Molly thought keeping their noses to the grindstone gave them a leg up on their high school peers."
}]
```
##### Success Response
```sh
{
    "name": "Booksmart 2",
    "description": "The academic overachievers Amy and Molly thought keeping their noses to the grindstone gave them a leg up on their high school peers.",
    "duration": 90,
    "uploadedDate": "2020-04-20T16:39:41.1347234",
    "isAgeRestricted": false,
    "category": "Comedy"
}
```

<br/>

# Users

| Method | Endpoint |
| ------ | ------ |
| **GET** | */vimtube/users* |
##### Request
```sh
None
```
##### Success Response
```sh
[
    {
        "email": "lucas@hotmail.com",
        "username": "Lucas"
    },
    {
        "email": "david@hotmail.com",
        "username": "David"
    }
]
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **GET** | */vimtube/users/1* |
##### Request
```sh
None
```
##### Success Response
```sh
{
    "email": "lucas@hotmail.com",
    "username": "Lucas"
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **POST** | */vimtube/users/add* |
##### Request
```sh
{
    "email": "lucas@hotmail.com",
    "username": "Lucas",
    "password": "lucas1234"
}
```
##### Success Response
```sh
{
    "email": "lucas@hotmail.com",
    "username": "Lucas"
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **PUT** | */vimtube/users/1* |
##### Request
```sh
{
    "email": "lucas@hotmail.com",
    "username": "Lucas",
    "password": "lucas1234"
}
```
##### Success Response
```sh
{
    "email": "lucas@hotmail.com",
    "username": "Lucas"
}
```

<br/>
<hr>
<br/>

| Method | Endpoint |
| ------ | ------ |
| **DELETE** | */vimtube/users/1* |
##### Request
```sh
None
```
##### Success Response
```sh
None
```