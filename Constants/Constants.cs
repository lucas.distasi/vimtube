﻿/*
 * Lucas Di Stasi - 2020
 */
namespace VimTube.Constants
{
    enum Constants
    {
        ADMIN = 1,
        VIEWER = 2
    }
}
