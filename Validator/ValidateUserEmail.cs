﻿/*
 * Lucas Di Stasi - 2020
 */
using System.ComponentModel.DataAnnotations;
using VimTube.Models;

namespace VimTube.Validator
{
    public class ValidateUserEmail : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var user = (User)validationContext.ObjectInstance;
            var address = new EmailAddressAttribute();

            if (address.IsValid(user.Email))
            {
                return ValidationResult.Success;
            }
            
            return new ValidationResult(ErrorMessage, new[] { "User" });
        }
    }
}
