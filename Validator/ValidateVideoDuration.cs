﻿/*
 * Lucas Di Stasi - 2020
 */
using System.ComponentModel.DataAnnotations;
using VimTube.Entities;

namespace VimTube.Validator
{
    public class ValidateVideoDuration : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var video = (Video)validationContext.ObjectInstance;

            if (video.Duration > 300 || video.Duration < 1)
            {
                return new ValidationResult( ErrorMessage, new[] { "Video" } );
            }

            return ValidationResult.Success;
        }
    }
}
