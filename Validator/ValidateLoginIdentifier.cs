﻿/*
 * Lucas Di Stasi - 2020
 */
using System.ComponentModel.DataAnnotations;
using VimTube.Models;

namespace VimTube.Validator
{
    public class ValidateLoginIdentifier : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var user = (User)validationContext.ObjectInstance;

            if (string.IsNullOrEmpty(user.Email) && string.IsNullOrEmpty(user.Username))
            {
                return new ValidationResult(ErrorMessage, new[] { "User" });
            }

            return ValidationResult.Success;
        }
    }
}
