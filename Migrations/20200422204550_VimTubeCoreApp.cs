﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VimTube.Migrations
{
    public partial class VimTubeCoreApp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CATEGORIES",
                columns: table => new
                {
                    IdCategory = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CATEGORIES", x => x.IdCategory);
                });

            migrationBuilder.CreateTable(
                name: "ROLES",
                columns: table => new
                {
                    IdRole = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleType = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ROLES", x => x.IdRole);
                });

            migrationBuilder.CreateTable(
                name: "VIDEOS",
                columns: table => new
                {
                    IdVideo = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(50)", nullable: false),
                    Description = table.Column<string>(maxLength: 250, nullable: false),
                    Duration = table.Column<int>(nullable: false),
                    UploadedDate = table.Column<DateTime>(nullable: false),
                    IsAgeRestricted = table.Column<bool>(nullable: false),
                    IdCategory = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VIDEOS", x => x.IdVideo);
                    table.ForeignKey(
                        name: "FK_VIDEOS_CATEGORIES_IdCategory",
                        column: x => x.IdCategory,
                        principalTable: "CATEGORIES",
                        principalColumn: "IdCategory",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "USERS",
                columns: table => new
                {
                    IdUser = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(maxLength: 59, nullable: false),
                    Username = table.Column<string>(maxLength: 59, nullable: false),
                    PasswordHash = table.Column<byte[]>(maxLength: 130, nullable: false),
                    PasswordSalt = table.Column<byte[]>(maxLength: 258, nullable: false),
                    IdRole = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USERS", x => x.IdUser);
                    table.ForeignKey(
                        name: "FK_USERS_ROLES_IdRole",
                        column: x => x.IdRole,
                        principalTable: "ROLES",
                        principalColumn: "IdRole",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ROLES",
                columns: new[] { "IdRole", "RoleType" },
                values: new object[] { 1, "ADMIN" });

            migrationBuilder.InsertData(
                table: "ROLES",
                columns: new[] { "IdRole", "RoleType" },
                values: new object[] { 2, "VIEWER" });

            migrationBuilder.InsertData(
                table: "USERS",
                columns: new[] { "IdUser", "Email", "IdRole", "PasswordHash", "PasswordSalt", "Username" },
                values: new object[] { 1, "lucas@hotmail.com", 1, new byte[] { 235, 165, 196, 34, 50, 148, 130, 120, 46, 224, 19, 198, 108, 177, 102, 46, 50, 96, 88, 203, 83, 77, 81, 68, 164, 38, 229, 42, 220, 97, 246, 61, 199, 169, 135, 22, 218, 157, 206, 184, 76, 82, 206, 192, 25, 224, 87, 169, 197, 25, 41, 49, 37, 134, 117, 4, 49, 110, 191, 65, 220, 135, 2, 114 }, new byte[] { 90, 105, 7, 223, 66, 165, 209, 89, 7, 235, 18, 183, 0, 77, 17, 130, 114, 169, 223, 223, 199, 44, 238, 14, 241, 217, 42, 98, 183, 47, 234, 32, 172, 117, 208, 9, 22, 122, 232, 8, 19, 101, 13, 129, 141, 46, 79, 239, 56, 216, 118, 126, 173, 178, 196, 134, 55, 27, 39, 142, 205, 145, 122, 101, 167, 229, 203, 71, 97, 225, 230, 175, 227, 249, 78, 87, 199, 62, 182, 109, 154, 63, 208, 15, 193, 190, 20, 147, 51, 234, 100, 120, 195, 199, 133, 36, 191, 170, 168, 13, 216, 59, 80, 224, 7, 120, 69, 176, 105, 233, 71, 240, 201, 14, 151, 242, 189, 106, 64, 140, 147, 61, 157, 147, 241, 130, 237, 222 }, "Lucas" });

            migrationBuilder.InsertData(
                table: "USERS",
                columns: new[] { "IdUser", "Email", "IdRole", "PasswordHash", "PasswordSalt", "Username" },
                values: new object[] { 2, "david@hotmail.com", 2, new byte[] { 239, 177, 196, 197, 147, 185, 57, 30, 58, 185, 230, 145, 5, 176, 1, 44, 235, 1, 17, 109, 248, 148, 156, 234, 92, 240, 242, 178, 127, 193, 255, 175, 1, 107, 37, 163, 247, 216, 43, 83, 173, 252, 161, 147, 76, 4, 53, 155, 183, 160, 210, 191, 206, 42, 241, 19, 237, 242, 216, 204, 121, 94, 28, 206 }, new byte[] { 90, 105, 7, 223, 66, 165, 209, 89, 7, 235, 18, 183, 0, 77, 17, 130, 114, 169, 223, 223, 199, 44, 238, 14, 241, 217, 42, 98, 183, 47, 234, 32, 172, 117, 208, 9, 22, 122, 232, 8, 19, 101, 13, 129, 141, 46, 79, 239, 56, 216, 118, 126, 173, 178, 196, 134, 55, 27, 39, 142, 205, 145, 122, 101, 167, 229, 203, 71, 97, 225, 230, 175, 227, 249, 78, 87, 199, 62, 182, 109, 154, 63, 208, 15, 193, 190, 20, 147, 51, 234, 100, 120, 195, 199, 133, 36, 191, 170, 168, 13, 216, 59, 80, 224, 7, 120, 69, 176, 105, 233, 71, 240, 201, 14, 151, 242, 189, 106, 64, 140, 147, 61, 157, 147, 241, 130, 237, 222 }, "David" });

            migrationBuilder.CreateIndex(
                name: "IX_CATEGORIES_Name",
                table: "CATEGORIES",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ROLES_RoleType",
                table: "ROLES",
                column: "RoleType",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_USERS_Email",
                table: "USERS",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_USERS_IdRole",
                table: "USERS",
                column: "IdRole");

            migrationBuilder.CreateIndex(
                name: "IX_USERS_Username",
                table: "USERS",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VIDEOS_IdCategory",
                table: "VIDEOS",
                column: "IdCategory");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "USERS");

            migrationBuilder.DropTable(
                name: "VIDEOS");

            migrationBuilder.DropTable(
                name: "ROLES");

            migrationBuilder.DropTable(
                name: "CATEGORIES");
        }
    }
}
