﻿/*
 * Lucas Di Stasi - 2020
 */
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VimTube.Entities
{
    [Table("ROLES")]
    public class Role
    {
        [Key]
        public int IdRole { get; set; }

        [Required]
        public string RoleType { get; set;  }

        public List<User> Users { get; set; }
    }
}
