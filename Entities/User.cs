﻿/*
 * Lucas Di Stasi - 2020
 */
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VimTube.Entities
{
    [Table("USERS")]
    public class User
    {
        [Key]
        public int IdUser { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(59)]
        public string Email { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(59)]
        public string Username { get; set; }

        [Required]
        [MinLength(130)]
        [MaxLength(130)]
        public byte[] PasswordHash { get; set; }

        [Required]
        [MinLength(258)]
        [MaxLength(258)]
        public byte[] PasswordSalt { get; set; }

        [ForeignKey("IdRole")]
        public Role Role { get; set; }

        [Required]
        public int IdRole { get; set; }
    }
}
