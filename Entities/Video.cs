﻿/*
 * Lucas Di Stasi - 2020
 */
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VimTube.Validator;

namespace VimTube.Entities
{
    [Table("VIDEOS")]
    public class Video
    {
        [Key]
        public int IdVideo { get; set; }

        [Required]
        [MinLength(2)]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }

        [Required]
        [MinLength(10)]
        [MaxLength(250)]
        public string Description { get; set; }

        [Required]
        [ValidateVideoDuration(ErrorMessage = "Duration can not be longer than 300 minutes or shorter than 1 minute")]
        public int Duration { get; set; }

        public DateTime UploadedDate { get; set; }

        public bool IsAgeRestricted { get; set; }

        [ForeignKey("IdCategory")]
        public Category Category { get; set; }

        [Required]
        public int IdCategory { get; set; }
    }
}
