﻿/*
 * Lucas Di Stasi - 2020
 */
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VimTube.Entities
{
    [Table("CATEGORIES")]
    public class Category
    {
        [Key]
        public int IdCategory { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Name { get; set; }

        public List<Video> Videos { get; set; }
    }
}
