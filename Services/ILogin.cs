﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Mvc;
using VimTube.Entities;

namespace VimTube.Services
{
    interface ILogin
    {
        IActionResult Login(string indentifier, string password);
        IActionResult Authenticate(string identifier, string password);
        bool VerifyPasswordHash(string password, byte[] hashBd, byte[] saltBd);
        string GenerateUserToken(User user);
    }
}
