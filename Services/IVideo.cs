﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using VimTube.Entities;

namespace VimTube.Services
{
    interface IVideo
    {
        Task<ActionResult<Models.Video>> FindOne(int id);
        Task<ActionResult<IEnumerable<Video>>> FindAll();
        Task<ActionResult<Video>> AddVideo(Video video);
        Task<ActionResult<Video>> DeleteVideo(int id);
        Task<ActionResult<Video>> UpdateVideo(int id, Video video);
        Task<ActionResult<Video>> UpdateVideoDescription(int id, JsonPatchDocument<Video> description);
    }
}
