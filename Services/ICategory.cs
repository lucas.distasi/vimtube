﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using VimTube.Entities;

namespace VimTube.Services
{
    interface ICategory
    {
        Task<ActionResult<Models.Category>> FindOne(int id);
        Task<ActionResult<IEnumerable<Category>>> FindAll();
        Task<ActionResult<Category>> AddCategory(Category category);
        Task<ActionResult<Category>> DeleteCategory(int id);
        Task<ActionResult<Category>> UpdateCategory(int id, Category category);
        List<Models.Category> ListCategoriesWhichStartsWith(string name);
    }
}
