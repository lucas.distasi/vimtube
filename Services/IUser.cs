﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using VimTube.Entities;

namespace VimTube.Services
{
    interface IUser
    {
        Task<ActionResult<Models.User>> FindOne(int id);
        Task<ActionResult<IEnumerable<User>>> FindAll();
        Task<ActionResult<User>> AddUser(Models.User user);
        Task<ActionResult<User>> UpdateUser(int id, Models.User user);
        Task<ActionResult<User>> DeleteUser(int id);
    }
}
