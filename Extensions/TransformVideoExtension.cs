﻿/*
 * Lucas Di Stasi - 2020
 */
using System.Collections.Generic;
using VimTube.Entities;

namespace VimTube.Extensions
{
    public static class TransformVideoExtension
    {
        public static Models.Video TransformNewVideo(this Video video, string category)
        {
            return new Models.Video
            {
                Name = video.Name,
                Description = video.Description,
                Duration = video.Duration,
                UploadedDate = video.UploadedDate,
                IsAgeRestricted = video.IsAgeRestricted,
                Category = category
            };
        }

        public static List<Models.Video> TransformVideoList(this List<Video> videosEntity)
        {
            List<Models.Video> videosModel = new List<Models.Video>();

            foreach (Video video in videosEntity)
            {
                videosModel.Add(new Models.Video
                {
                    Name = video.Name,
                    Description = video.Description,
                    Duration = video.Duration,
                    UploadedDate = video.UploadedDate,
                    IsAgeRestricted = video.IsAgeRestricted,
                    Category = video.Category.Name
                });
            }

            return videosModel;
        }

        public static Models.Video TransformVideo(this Video video, string category)
        {
            return new Models.Video
            {
                Name = video.Name,
                Description = video.Description,
                Duration = video.Duration,
                UploadedDate = video.UploadedDate,
                IsAgeRestricted = video.IsAgeRestricted,
                Category = category
            };
        }
    }
}
