﻿/*
 * Lucas Di Stasi - 2020
 */
using System.Collections.Generic;
using VimTube.Entities;

namespace VimTube.Extensions
{
    public static class TransformUserExtension
    {
        public static Models.User TransformUser(this User user)
        {
            return new Models.User
            {
                Email = user.Email,
                Username = user.Username
            };
        }

        public static List<Models.User> TransformUsers(this List<User> users)
        {
            List<Models.User> usersModel = new List<Models.User>();

            foreach(User userEntity in users)
            {
                usersModel.Add(new Models.User
                {
                    Email = userEntity.Email,
                    Username = userEntity.Username
                });
            }

            return usersModel;
        }
    }
}
