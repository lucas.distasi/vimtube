﻿/*
 * Lucas Di Stasi - 2020
 */
using System.Security.Cryptography;
using System.Text;
using VimTube.Entities;

namespace VimTube.Extensions
{
    public static class TransformUserLoginExtension
    {
        public static User TransformUserRegistrer(this Models.User userModel)
        {
            byte[] passwordHash;
            byte[] passwordSalt;

            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(userModel.Password));
            }

            return new User()
            {
                Email = userModel.Email,
                Username = userModel.Username,
                PasswordSalt = passwordSalt,
                PasswordHash = passwordHash,
                IdRole = (int)Constants.Constants.VIEWER
            };
        }
    }
}
