﻿/*
 * Lucas Di Stasi - 2020
 */
using System.Collections.Generic;
using VimTube.Entities;

namespace VimTube.Extensions
{
    public static class TransformCategoryExtension
    {
        public static Models.Category TransformCategory(this Category category, List<Models.Video> videos)
        {
            return new Models.Category
            {
                Name = category.Name,
                Videos = videos
            };
        }

        public static Models.Category TransformNewCategory(this Category category)
        {
            return new Models.Category
            {
                Name = category.Name
            };
        }
    }
}
