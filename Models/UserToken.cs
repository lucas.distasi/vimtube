﻿/*
 * Lucas Di Stasi - 2020
 */
namespace VimTube.Models
{
    public class UserToken
    {
        public string Token { get; set; }
    }
}
