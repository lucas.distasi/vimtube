﻿/*
 * Lucas Di Stasi - 2020
 */
using System;

namespace VimTube.Models
{
    public class Video
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
        public DateTime UploadedDate { get; set; }
        public bool IsAgeRestricted { get; set; }
        public string Category { get; set; }
    }
}
