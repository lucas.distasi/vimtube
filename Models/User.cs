﻿/*
 * Lucas Di Stasi - 2020
 */
using System.ComponentModel.DataAnnotations;
using VimTube.Validator;

namespace VimTube.Models
{
    public class User
    {
        [ValidateLoginIdentifier]
        [ValidateUserEmail]
        public string Email { get; set; }

        [ValidateLoginIdentifier]
        public string Username { get; set; }

        [Required]
        [MinLength(6)]
        [MaxLength(24)]
        public string Password { get; set; }
    }
}
