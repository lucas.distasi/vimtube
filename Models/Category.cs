﻿/*
 * Lucas Di Stasi - 2020
 */
using System.Collections.Generic;

namespace VimTube.Models
{
    public class Category
    {
        public string Name { get; set; }
        public ICollection<Video> Videos { get; set; }
    }
}
