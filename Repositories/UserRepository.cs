﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Mvc;
using VimTube.Data;
using VimTube.Entities;
using VimTube.Extensions;
using VimTube.Services;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.AspNetCore.Http;

namespace VimTube.Repositories
{
    public class UserRepository : ControllerBase, IUser
    {
        private readonly VimTubeContext _context;
        private readonly ILogger<UserRepository> _logger;
        private readonly IHttpContextAccessor _httpContext;
        private readonly string user;

        public UserRepository(VimTubeContext context, ILogger<UserRepository> logger, IHttpContextAccessor httpContext)
        {
            _context = context;
            _logger = logger;
            _httpContext = httpContext;
            user = Utils.Utils.GetUser(_httpContext);
        }

        public async Task<ActionResult<IEnumerable<User>>> FindAll()
        {
            var users = await _context.Users.ToListAsync();

            return Ok(users.TransformUsers());
        }

        public async Task<ActionResult<Models.User>> FindOne(int id)
        {
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user.TransformUser());
        }

        public async Task<ActionResult<User>> AddUser(Models.User userModel)
        {
            User userEntity = userModel.TransformUserRegistrer();

            _context.Users.Add(userEntity);

            try
            {
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User [ {user} ] adde user {userEntity.Username} with email {userEntity.Email}.");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError($"User [ {user} ] failed adding user {userEntity.Username} with email {userEntity.Email}.");
                _logger.LogError($"{ex.Message}.");
                return BadRequest();
            }

            return CreatedAtAction("GetAllUsers", new { userEntity.IdUser }, userEntity.TransformUser());
        }

        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            
            try
            {
                User deletedUser = user;
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User [ {user} ] deleted user {deletedUser.Username} with email {deletedUser.Email}.");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError($"User [ {user} ] failed adding user {user.Username} with email {user.Email}.");
                _logger.LogError($"{ex.Message}.");
            }

            return NoContent();
        }

        public async Task<ActionResult<User>> UpdateUser(int id, Models.User userModel)
        {
            User userEntity = userModel.TransformUserRegistrer();

            userEntity.IdRole = Utils.Utils.IsUserAdmin(userEntity, _context) ? (int)Constants.Constants.ADMIN
                                                                              : (int)Constants.Constants.VIEWER;
            userEntity.IdUser = id;

            try
            {
                _context.Entry(userEntity).State = EntityState.Modified;
                _context.SaveChanges();
                _logger.LogInformation($"User [ {user} ] updated user with ID [ {userEntity.IdUser} ].");
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _context.Entry(userEntity).State = EntityState.Detached;
                var user = await _context.Users.FindAsync(id);

                if (null == user)
                {
                    return NotFound();
                }

                _logger.LogError($"User [ {user} ] failed updating user {userEntity.Username} with email {userEntity.Email}.");
                _logger.LogError($"{ex.Message}.");
                throw;
            }

            return CreatedAtAction("GetUser", new { id }, userEntity.TransformUser());
        }
    }
}
