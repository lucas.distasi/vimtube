﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VimTube.Data;
using VimTube.Entities;
using VimTube.Extensions;
using VimTube.Services;

namespace VimTube.Repositories
{
    public class VideoRepository : ControllerBase, IVideo
    {
        private readonly VimTubeContext _context;
        private readonly ILogger<VideoRepository> _logger;
        private readonly IHttpContextAccessor _httpContext;
        private readonly string user;

        public VideoRepository(VimTubeContext context, ILogger<VideoRepository> logger, IHttpContextAccessor httpContext)
        {
            _context = context;
            _logger = logger;
            _httpContext = httpContext;
            user = Utils.Utils.GetUser(_httpContext);
        }

        public async Task<ActionResult<Video>> AddVideo(Video video)
        {
            string u = user;

            if (null == _context.Categories.FindAsync(video.IdCategory).Result)
            {
                _logger.LogError($"User [ {user} ] failed adding a video. Category with id {video.IdCategory} was not found");
                return NotFound();
            }

            _context.Videos.Add(video);
            video.UploadedDate = DateTime.UtcNow;
            
            try
            {
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User [ {user} ] added video with name {video.Name} and category {FindCategoryName(video.IdCategory)}");
            }
            catch(DbUpdateException ex)
            {
                _logger.LogError($"User [ {user} ] failed adding a video with name {video.Name}");
                _logger.LogError($"Message => {ex.Message}");
                return BadRequest();
            }

            return CreatedAtAction("GetAllVideos", new { id = video.IdVideo }, video.TransformNewVideo(FindCategoryName(video.IdCategory)));
        }

        public async Task<ActionResult<Video>> DeleteVideo(int id)
        {
            var video = await _context.Videos.FindAsync(id);

            if (video == null)
            {
                return NotFound();
            }

            _context.Videos.Remove(video);
            
            try
            {
                string videoName = video.Name;
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User [ {user} ] deleted video with name {videoName}");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError($"User [ {user} ] failed deleting video with name {video.Name}");
                _logger.LogError($"Message => {ex.Message}");
                return BadRequest();
            }

            return NoContent();
        }

        public async Task<ActionResult<IEnumerable<Video>>> FindAll()
        {
            var videosEntity = await _context.Videos.ToListAsync();

            foreach (Video video in videosEntity)
            {
                video.Category = new Category
                {
                    Name = FindCategoryName(video.IdCategory)
                };
            }

            return Ok(videosEntity.TransformVideoList());
        }

        public async Task<ActionResult<Models.Video>> FindOne(int id)
        {
            var video = await _context.Videos.FindAsync(id);

            if (video == null)
            {
                return NotFound();
            }

            return video.TransformVideo(FindCategoryName(video.IdCategory));
        }

        public async Task<ActionResult<Video>> UpdateVideo(int id, Video video)
        {
            var category = _context.Categories.FindAsync(video.IdCategory);

            if (null == category.Result)
            {
                return NotFound();
            }

            try
            {
                video.UploadedDate = DateTime.UtcNow;
                _context.Entry(video).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User [ {user} updated video with ID [ {video.Name} ].");
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _context.Entry(video).State = EntityState.Detached;
                var vid = await _context.Videos.FindAsync(id); // esto deberia ser null

                if (null == vid)
                {
                    return NotFound();
                }

                _logger.LogError($"User [ {user} ] failed updating video with name {video.Name}");
                _logger.LogError($"Message => {ex.Message}");
                return BadRequest();
            }

            return CreatedAtAction("GetVideo", new { id }, video.TransformNewVideo(FindCategoryName(video.IdCategory)));
        }

        //Add validation for invalid path, op, null value - maybe add validation to avoid updating other video fields
        public async Task<ActionResult<Video>> UpdateVideoDescription(int id, JsonPatchDocument<Video> descriptionPatch)
        {
            var video = await _context.Videos.FindAsync(id);

            if (video == null)
            {
                return NotFound();
            }

            descriptionPatch.ApplyTo(video);
            _context.Videos.Update(video);

            try
            {
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User [ {user} ] updated video description with ID [ {video.IdVideo} ].");
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (null == FindOne(id))
                {
                    return NotFound();
                }

                _logger.LogError($"User [ {user} ] failed updating video description for video with name {video.Name}");
                _logger.LogError($"Message => {ex.Message}");

                return BadRequest();
            }

            return Ok(video.TransformVideo(FindCategoryName(video.IdCategory)));
        }

        public string FindCategoryName(int id)
        {
            return _context.Categories.Find(id).Name;
        }
    }
}
