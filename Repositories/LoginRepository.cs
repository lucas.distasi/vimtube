﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using VimTube.Data;
using VimTube.Entities;
using VimTube.Services;

namespace VimTube.Repositories
{
    public class LoginRepository : ControllerBase, ILogin
    {
        private readonly VimTubeContext _context;
        private readonly IConfiguration _config;
        private readonly ILogger<LoginRepository> _logger;
        private readonly IHttpContextAccessor _httpContext;
        private readonly string user;

        public LoginRepository(VimTubeContext context, IConfiguration config, 
                                ILogger<LoginRepository> logger, IHttpContextAccessor httpContext)
        {
            _context = context;
            _config = config;
            _logger = logger;
            _httpContext = httpContext;
            user = Utils.Utils.GetUser(_httpContext);
        }

        public IActionResult Login(string identifier, string password)
        {
            if (string.IsNullOrEmpty(identifier) || string.IsNullOrEmpty(password))
            {
                return BadRequest();
            }

            _logger.LogInformation($"User {identifier} trying to login ...");
            return Authenticate(identifier, password);
        }

        public IActionResult Authenticate(string identifier, string password)
        {
            User user = _context.Users.Where(databaseUser => databaseUser.Email.Equals(identifier) || databaseUser.Username.Equals(identifier)).Single();

            if (user == null)
            {
                return NotFound();
            }

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                _logger.LogError($"User {identifier} failed to login ...");
                return BadRequest("Bad credentials");
            }

            string token = GenerateUserToken(user).ToString();
            Models.UserToken userToken = new Models.UserToken
            {
                Token = token
            };

            _logger.LogInformation($"User {user.Username} with email {user.Email} has logged in ...");
            return Ok(token);
        }

        public bool VerifyPasswordHash(string password, byte[] databaseHash, byte[] databaseSalt)
        {
            ValidatePasswords(password, databaseHash, databaseSalt);

            using (var hmac = new HMACSHA512(databaseSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != databaseHash[i]) return false;
                }
            }

            return true;
        }

        public string GenerateUserToken(User userEntity)
        {
            var user = _context.Users.Where(databaseUser => databaseUser.Email.Equals(userEntity.Email) || databaseUser.Username.Equals(userEntity.Username)).First();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtConfig:secret"]));
            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, _config["JwtConfig:Subject"]),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    new Claim("Id", user.IdUser.ToString()),
                    new Claim("Email", user.Email),
                    new Claim("UserName", user.Username),
                    new Claim("roles", Utils.Utils.IsUserAdmin(userEntity, _context) ? "ADMIN" : "VIEWER" )
                   };
            var token = new JwtSecurityToken(_config["JwtConfig:Issuer"], _config["JwtConfig:Audience"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);
            var valueToken = new JwtSecurityTokenHandler().WriteToken(token);

            return valueToken;
        }

        public void ValidatePasswords(string password, byte[] databaseHash, byte[] databaseSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (databaseHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (databaseSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordSalt");
        }
    }
}
