﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VimTube.Data;
using VimTube.Entities;
using VimTube.Extensions;
using VimTube.Services;

namespace VimTube.Repositories
{
    public class CategoryRespository : ControllerBase, ICategory
    {
        private readonly VimTubeContext _context;
        private readonly ILogger<CategoryRespository> _logger;
        private readonly IHttpContextAccessor _httpContext;
        private readonly string user;

        public CategoryRespository(VimTubeContext context, ILogger<CategoryRespository> logger, IHttpContextAccessor httpContext)
        {
            _context = context;
            _logger = logger; 
            _httpContext = httpContext;
            user = Utils.Utils.GetUser(_httpContext);
        }

        public async Task<ActionResult<Category>> AddCategory(Category category)
        {
            _context.Categories.Add(category);

            try
            {
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User [ {user} ] added category {category.Name}.");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError($"User [ {user} ] failed adding a category with name {category.Name}.");
                _logger.LogError($"Error message => {ex.Message}");
                return BadRequest();
            }

            return CreatedAtAction("GetAllCategories", new { id = category.IdCategory }, category.TransformNewCategory());
        }

        public async Task<ActionResult<Category>> DeleteCategory(int id)
        {
            var category = await _context.Categories.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            _context.Categories.Remove(category);
            
            try
            {
                await _context.SaveChangesAsync();
                _logger.LogInformation($"User [ {user} ] removed category {category.Name}.");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError($"User [ {user} ] failed removing category {category.Name}.");
                _logger.LogError($"Error message => {ex.Message}");
                return BadRequest();
            }

            return NoContent();
        }

        public async Task<ActionResult<IEnumerable<Category>>> FindAll()
        {
            var entities = await _context.Categories.ToListAsync();
            var categories = new List<Models.Category>();

            foreach (var category in entities)
            {
                categories.Add(new Models.Category
                {
                    Name = category.Name,
                    Videos = ListVideosWithCategory(category.IdCategory)
                });
            }

            return Ok(categories);
        }

        public async Task<ActionResult<Models.Category>> FindOne(int id)
        {
            var category = await _context.Categories.FindAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            return category.TransformCategory(ListVideosWithCategory(category.IdCategory));
        }

        public async Task<ActionResult<Category>> UpdateCategory(int id, Category category)
        {
            try
            {
                string oldCategoryName = category.Name;

                _context.Entry(category).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                _logger.LogInformation($"User [ {user} ] updated category {oldCategoryName}. New name is {category.Name}.");
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _context.Entry(category).State = EntityState.Detached;
                var categoryExists = await _context.Categories.FindAsync(id);

                if (null == categoryExists)
                {
                    return NotFound();
                }

                _logger.LogError($"User [ {user} ] failed updating category {category.Name}.");
                _logger.LogError($"Error message => {ex.Message}");

                return BadRequest();
            }

            return CreatedAtAction("GetCategory", new { id }, category.TransformNewCategory());
        }

        public List<Models.Category> ListCategoriesWhichStartsWith(string name)
        {
            name = name.Trim();
            var entityCategories = _context.Categories.Where(n => n.Name.StartsWith(name));
            List<Models.Category> modelCategories = new List<Models.Category>();

            foreach (var category in entityCategories)
            {
                modelCategories.Add(new Models.Category
                {
                    Name = category.Name,
                    Videos = ListVideosWithCategory(category.IdCategory)
                });
            }

            return modelCategories;
        }

        public List<Models.Video> ListVideosWithCategory(int id)
        {
            var entityVideos = _context.Videos.Where(video => video.IdCategory == id);
            List<Models.Video> videos = new List<Models.Video>();

            foreach (Video video in entityVideos)
            {
                videos.Add(new Models.Video
                {
                    Name = video.Name,
                    Description = video.Description,
                    Duration = video.Duration,
                    UploadedDate = video.UploadedDate,
                    IsAgeRestricted = video.IsAgeRestricted
                });
            }

            return videos;
        }
    }
}
