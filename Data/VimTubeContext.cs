﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using VimTube.Entities;

namespace VimTube.Data
{
    public class VimTubeContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }

        public VimTubeContext(DbContextOptions<VimTubeContext> options) : base (options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            HMAC hmac = new HMACSHA512();

            byte[] passwordSaltAdmin = hmac.Key;
            byte[] passwordHashAdmin = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes("LucasD"));

            byte[] passwordSaltViewer = hmac.Key;
            byte[] passwordHashViewer = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes("DavidL"));

            modelBuilder.Entity<Category>().HasIndex(n => n.Name).IsUnique();

            modelBuilder.Entity<User>().HasData(
                new User { IdUser = 1, Email = "lucas@hotmail.com", Username = "LucasD", PasswordHash = passwordHashAdmin, PasswordSalt = passwordSaltAdmin, IdRole = 1 },
                new User { IdUser = 2, Email = "david@hotmail.com", Username = "DavidL", PasswordHash = passwordHashViewer, PasswordSalt = passwordSaltViewer, IdRole = 2 }
                );
            modelBuilder.Entity<User>().HasIndex(e => e.Email).IsUnique();
            modelBuilder.Entity<User>().HasIndex(u => u.Username).IsUnique();

            modelBuilder.Entity<Role>().HasData(
                new Role { IdRole = 1, RoleType = "ADMIN" },
                new Role { IdRole = 2, RoleType = "VIEWER" }
                );
            modelBuilder.Entity<Role>().HasIndex(r => r.RoleType).IsUnique();
        }
    }
}
