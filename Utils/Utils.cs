﻿/*
 * Lucas Di Stasi - 2020
 */
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using VimTube.Data;
using VimTube.Entities;

namespace VimTube.Utils
{
    public static class Utils
    {
        public static bool IsUserAdmin(User user, VimTubeContext _context)
        {
            return FindRoleByRoleId(user.IdRole, _context).Result.Value.RoleType.Equals("ADMIN");
        }

        public static async Task<ActionResult<Role>> FindRoleByRoleId(int idRole, VimTubeContext _context)
        {
            var role = await _context.Roles.FindAsync(idRole);
            return role;
        }

        public static string GetUser(IHttpContextAccessor _httpContext)
        {
            StringValues headerValues;
            var tokenHeader = _httpContext.HttpContext.Request.Headers.TryGetValue("Authorization", out headerValues);
            var formatedToken = headerValues.ToString().Replace("{", "").Replace("}", "").Replace("Bearer", "").Trim();

            if(string.IsNullOrEmpty(formatedToken))
            {
                return "Anonymous";
            }

            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(formatedToken);
            var user = token.Claims.ElementAt(5).Value;

            return user;
        }
    }
}
